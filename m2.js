/**
 *This is the Object
 * Object => key , value pair in js
 */
const person = {
    name: "Rahul",
    age: 23,
    walk() {
    },

}

/*
    when we don't know the property of an object we use bracket.
    when we know the property of an object we use dot notation.
 */


console.log(person.age)
const targetMember = "name"
console.log(person[targetMember])



