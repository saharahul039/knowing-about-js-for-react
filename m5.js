/*
    Arrow ---------->function
 */

// normal function
const square = function(number){
    return number*number
}
console.log(square(5))

// converting arrow function
const sq = (number) =>{
    return number*number
}
// arrow function
console.log(sq(5))
