/*
    arrow function and this
 */
const person = {

    talk()
    {
        var self = this
        setTimeout(function(){
            console.log(self) //here this return Timeout obj
        }, 1000)

    }
}
person.talk()