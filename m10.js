//class
class Person{
    // constructor fun
    constructor (name){
        this.name = name
    }

    walk(){
        console.log("walking...")
    }

    stop()
    {
        console.log("stop now")
    }
}

const p = new Person("Rahul")
p.walk()
p.stop()



/*
    Inheritance of the class
 */

class Teacher extends Person{
    constructor(name, degree)
    {
        super(name)
        this.degree = degree
    }
    tech(){
        console.log("Teaching...")
    }
}

const t = new Teacher("Rahul")
t.walk()
t.tech()
console.log(t.name)