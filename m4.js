/*
    binding this
 */
const person = {
    name:"RAHUL SAHA",
    type:"STU",
    walk(){
        console.log(this)
    },
}

/*
    bind( function )  =>  bind a function to an object.

 */

const walk = person.walk.bind(person)


/*
    this => global object
    How this => person object
 */
walk()