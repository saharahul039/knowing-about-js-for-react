/*
    new method in Array => map
    map( callback_function )
 */
const colors = ["blue", "green", "yellow", "red", "black"]

const c =  colors.map(color=>`<li>${color}</li>`)
console.log(c)