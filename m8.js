/*
    Object destructing

 */

const address = {
    street:"G_pattern",
    city:"C_pattern",
}

const street1 = address.street
const city1 = address.city

// using object dest
const { street, city} = address;
console.log(street)
