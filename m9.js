/*
    spread operator
    Array m=> concat [two array joined]
 */

const first = [4,5,6,7]
const second = [2,3,4,5]

const com = first.concat(second)
console.log(com)

// using spread operator
const c = [...first, ...second, 40,50]
console.log(c)


// for object
const b = {
    name:"Mosh"
}
const d = {
    name:"Bob"
}

const comb = {...b, ...d}
console.log(comb)